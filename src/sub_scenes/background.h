#ifndef BACKGROUND_H
#define BACKGROUND_H

namespace game
{
	namespace background
	{
		void defineImagesPosition();

		void setImagesMovement();

		void increaseBackgroundSpeed();

		void drawScenario();
	}
}

#endif // BACKGROUND_H
