#include "background.h"

//#include "game/game.h"
#include "game_vars/global_vars.h"
#include "resourses/textures.h"

using namespace game;
using namespace global_vars;
using namespace textures;

namespace game
{
	namespace background
	{
		void defineImagesPosition()
		{
			backgroundScenario[0].position = 0;
			backgroundScenario[1].position = screenWidth;
			backgroundScenario[2].position = -screenWidth;
			middleScenario[0].position = 0;
			middleScenario[1].position = screenWidth;
			middleScenario[2].position = -screenWidth;
			fowardScenario[0].position = 0;
			fowardScenario[1].position = screenWidth;
			fowardScenario[2].position = -screenWidth;

			backgroundScenario[0].onSight = true;
			backgroundScenario[1].onSight = false;
			backgroundScenario[2].onSight = false;
			middleScenario[0].onSight = true;
			middleScenario[1].onSight = false;
			middleScenario[2].onSight = false;
			fowardScenario[0].onSight = true;
			fowardScenario[1].onSight = false;
			fowardScenario[2].onSight = false;

			backgroundScenario[0].speed = 1.0f;
			middleScenario[0].speed = 1.5f;
			fowardScenario[0].speed = 2.0f;

			floorScenario[0].position = 0;
			floorScenario[1].position = screenWidth;
			floorScenario[2].position = -screenWidth;

			floorScenario[0].onSight = true;
			floorScenario[1].onSight = false;
			floorScenario[2].onSight = false;

			floorScenario[0].speed = 4.0f;
		}

		void setImagesMovement()
		{
			for (short i = 0; i < backgroundImages; i++)
			{
				backgroundScenario[i].position -= backgroundScenario[0].speed * 60 * GetFrameTime();
				middleScenario[i].position -= middleScenario[0].speed * 60 * GetFrameTime();
				fowardScenario[i].position -= fowardScenario[0].speed * 60 * GetFrameTime();
				floorScenario[i].position -= floorScenario[0].speed * 60 * GetFrameTime();
			}

			for (short i = 0; i < backgroundImages; i++)
			{
				switch (i)
				{
				case 0:
					if (backgroundScenario[i].position <= -screenWidth)
						backgroundScenario[i].position = backgroundScenario[i + 2].position + screenWidth - 1;
					if (middleScenario[i].position <= -screenWidth)
						middleScenario[i].position = middleScenario[i + 2].position + screenWidth - 1;
					if (fowardScenario[i].position <= -screenWidth)
						fowardScenario[i].position = fowardScenario[i + 2].position + screenWidth - 1;
					if (floorScenario[i].position <= -screenWidth)
						floorScenario[i].position = floorScenario[i + 2].position + screenWidth - 1;
					break;
				case 1:
				case 2:
					if (backgroundScenario[i].position <= -screenWidth)
						backgroundScenario[i].position = backgroundScenario[i - 1].position + screenWidth - 1;
					if (middleScenario[i].position <= -screenWidth)
						middleScenario[i].position = middleScenario[i - 1].position + screenWidth - 1;
					if (fowardScenario[i].position <= -screenWidth)
						fowardScenario[i].position = fowardScenario[i - 1].position + screenWidth - 1;
					if (floorScenario[i].position <= -screenWidth)
						floorScenario[i].position = floorScenario[i - 1].position  + screenWidth - 1;
					break;
				default:
					break;
				}
			}
		}

		void increaseBackgroundSpeed()
		{
			backgroundScenario[0].speed += 0.0750f;
			middleScenario[0].speed += 0.1125f;
			fowardScenario[0].speed += 0.15f;

			floorScenario[0].speed += 0.3f;			
		}

		void drawScenario()
		{
			for (short i = 0; i < backgroundImages; i++)
			{
				DrawTextureEx(gameplayBackground[i].texture, { static_cast<float>(backgroundScenario[i].position), 0.0f }, 0.0f, 1.0f, WHITE);
			}

			for (short i = 0; i < backgroundImages; i++)
			{
				DrawTextureEx(gameplayMiddle[i].texture, { static_cast<float>(middleScenario[i].position), 0.0f }, 0.0f, 1.0f, WHITE);
			}

			for (short i = 0; i < backgroundImages; i++)
			{
				DrawTextureEx(gameplayFoward[i].texture, { static_cast<float>(fowardScenario[i].position), 0.0f }, 0.0f, 1.0f, WHITE);
			}

			for (short i = 0; i < backgroundImages; i++)
			{
				DrawTextureEx(floor[i].texture, { static_cast<float>(floorScenario[i].position), 0.0f }, 0.0f, 1.0f, WHITE);
			}
		}
	}
}