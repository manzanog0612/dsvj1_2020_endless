#ifndef OBJECTS_H
#define OBJECTS_H

namespace game
{
	namespace objects
	{
		void defineObjects();

		void setObjectsUpdate();

		void drawObject();
	}
}

#endif // OBJECTS_H
