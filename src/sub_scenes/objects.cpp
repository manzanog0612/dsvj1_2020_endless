#include "objects.h"

#include <stdlib.h> 

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace global_vars;
using namespace global_drawing_vars;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace objects
	{
		void defineObjects()
		{
			short treasureAux;
			short obstaclesAux;

			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					treasureAux = rand() % 3;
					obstaclesAux = rand() % 3;
					objectsInGame[i][j].inGame = true;
					objectsInGame[i][j].isObstacle = false;

					objectsInGame[i][j].dimentions.y = floorsPosY[i];

					objectsInGame[i][j].actualFloor = static_cast<LINES>(i);

					objectsInGame[i][j].dimentions.x = static_cast<float>(screenWidth + screenWidth / 3.5f * j);
					
					if (i != 0)
					{
						if (!objectsInGame[i - 1][j].isObstacle && rand() % 2 == 1)
						{
							objectsInGame[i][j].texture = obstaclesT[obstaclesAux].texture;
							objectsInGame[i][j].obstacleType = static_cast<OBSTACLES>(obstaclesAux);
							objectsInGame[i][j].isObstacle = true;
						}
						else
						{
							objectsInGame[i][j].texture = treasuresT[treasureAux].texture;
							objectsInGame[i][j].treasureType = static_cast<TREASURES>(treasureAux);
							objectsInGame[i][j].isObstacle = false;
						}
					} 
					else if (rand() % 2 == 1)
					{
						objectsInGame[i][j].texture = obstaclesT[obstaclesAux].texture;
						objectsInGame[i][j].obstacleType = static_cast<OBSTACLES>(obstaclesAux);
						objectsInGame[i][j].isObstacle = true;
					}
					else
					{
						objectsInGame[i][j].texture = treasuresT[treasureAux].texture;
						objectsInGame[i][j].treasureType = static_cast<TREASURES>(treasureAux);
						objectsInGame[i][j].isObstacle = false;
					}

					if (!objectsInGame[i][j].isObstacle)
					{
						objectsInGame[i][j].dimentions.width = treasuresT[treasureAux].width;
						objectsInGame[i][j].dimentions.height = treasuresT[treasureAux].height;
						objectsInGame[i][j].dimentions.y -= treasuresT[treasureAux].height * 1.5f;
					}
					else
					{
						objectsInGame[i][j].dimentions.width = obstaclesT[obstaclesAux].width;
						objectsInGame[i][j].dimentions.height = obstaclesT[obstaclesAux].height;
						objectsInGame[i][j].dimentions.y -= obstaclesT[obstaclesAux].height;
					}
				}
			}
		}

		static void setSoundCollisionTreasure()
		{
			treasureTouched = true;
			soundCollisionTreasurePlayed = true;
		}

		static void setSoundCollisionObstacle()
		{
			obstacleTouched = true;
			soundCollisionObstaclePlayed = true;
		}

		static void moveObjects()
		{
			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					objectsInGame[i][j].dimentions.x -= floorScenario[0].speed * 60 * GetFrameTime();;
				}
			}
		}

		static void deactivateObjects()
		{
			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					if (objectsInGame[i][j].dimentions.x <= screenLimit.left - screenWidth / 10)
					{
						objectsInGame[i][j].inGame = false;
					}
				}
			}
		}

		static bool checkAnyObjectsClose(short floor)
		{
			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					if (objectsInGame[i][j].actualFloor == static_cast<LINES>(floor))
					{
						if (objectsInGame[i][j].dimentions.x + objectsInGame[i][j].dimentions.width > screenLimit.right - screenWidth / 4)
							return true;
					}
				}
			}

			return false;
		}

		static bool checkAnyObstacleClose(short floor)
		{
			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					if (objectsInGame[i][j].actualFloor == static_cast<LINES>(floor))
					{
						if (objectsInGame[i][j].dimentions.x + objectsInGame[i][j].dimentions.width > screenLimit.right - screenWidth / 4 &&
							objectsInGame[i][j].isObstacle)
							return true;
					}
				}
			}

			return false;
		}

		static void reactivateObjects()
		{
			short obstaclesAux;
			TREASURES treasureAux;

			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					obstaclesAux = rand() % 3;

					if (!objectsInGame[i][j].inGame)
					{
						for (short k = 0; k < amountFloors; k++)
						{
							if (!checkAnyObjectsClose(k))
							{
								if (!objectsInGame[i][j].isObstacle || ((k == 0 && rand() % 3 == 1) || 
									( k != 0 && !checkAnyObstacleClose(k - 1))))
								{
									objectsInGame[i][j].inGame = true;
									objectsInGame[i][j].dimentions.x = static_cast<float>(screenLimit.right);
									objectsInGame[i][j].dimentions.y = floorsPosY[k];
									objectsInGame[i][j].actualFloor = static_cast<LINES>(k);

									if (objectsInGame[i][j].isObstacle)
									{
										objectsInGame[i][j].texture = obstaclesT[obstaclesAux].texture;
										objectsInGame[i][j].obstacleType = static_cast<OBSTACLES>(obstaclesAux);
										objectsInGame[i][j].dimentions.width = obstaclesT[obstaclesAux].width;
										objectsInGame[i][j].dimentions.height = obstaclesT[obstaclesAux].height;
										objectsInGame[i][j].dimentions.y -= obstaclesT[obstaclesAux].height;
									}
									else
									{
										treasureAux = objectsInGame[i][j].treasureType;
										objectsInGame[i][j].dimentions.y -= treasuresT[static_cast<int>(treasureAux)].height * 1.5f;
									}
								}
							}
						}
					}
				}
			}
		}

		static void addScore(short objRow, short objLine)
		{
			switch (objectsInGame[objRow][objLine].treasureType)
			{
			case TREASURES::FISH:
				player.score += 1;
				break;
			case TREASURES::WOOL:
				player.score += 3;
				break;
			case TREASURES::MILK:
				player.score += 5;
				break;
			default:
				break;
			}
		}

		static void checkColiisionWithPlayer()
		{
			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					if (objectsInGame[i][j].inGame)
					{
						if (CheckCollisionRecs(player.dimentions, objectsInGame[i][j].dimentions))
						{
							if (!objectsInGame[i][j].isObstacle)
							{
								treasureTouched = true;
								objectsInGame[i][j].inGame = false;
								addScore(i, j);
							}
							else
							{
								switch (objectsInGame[i][j].obstacleType)
								{
								case OBSTACLES::BATHTUB:
									lose = true;
									break;
								case OBSTACLES::DOG:
								case OBSTACLES::MADCAT:
									if (dashInProcess)
									{
										obstacleTouched = true;
										objectsInGame[i][j].inGame = false;
									}
									else
										lose = true;
									break;
								default:
									break;
								}
							}
						}
					}
				}
			}
		}

		void setObjectsUpdate()
		{
			moveObjects();
			checkColiisionWithPlayer();
			deactivateObjects();
			reactivateObjects();
		}

		void drawObject()
		{
			for (short i = 0; i < amountObjectsY; i++)
			{
				for (short j = 0; j < amountObjectsX; j++)
				{
					if (objectsInGame[i][j].inGame)
					{
						if (objectsInGame[i][j].isObstacle && objectsInGame[i][j].obstacleType == OBSTACLES::BATHTUB)
							DrawTextureEx(objectsInGame[i][j].texture, { objectsInGame[i][j].dimentions.x, objectsInGame[i][j].dimentions.y }, 
							0.0f, 1.0f, WHITE);
						else
							DrawTextureEx(objectsInGame[i][j].texture, { objectsInGame[i][j].dimentions.x, objectsInGame[i][j].dimentions.y },
								0.0f, 1.0f, WHITE);

						#if DEBUG
						if (objectsInGame[i][j].isObstacle)
							DrawRectangleLinesEx(objectsInGame[i][j].dimentions, 2, RED);
						else
							DrawRectangleLinesEx(objectsInGame[i][j].dimentions, 2, GREEN);
						#endif // DEBUG
					}
				}
			}
		}		
	}
}