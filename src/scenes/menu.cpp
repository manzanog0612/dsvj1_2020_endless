#include "scenes/menu.h"

#include "scenes/gameplay.h"
#include "scenes/rules.h"
#include "scenes/credits.h"
#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace menu
	{
		static void drawingVarsInitialization()
		{
			playOp.text = "Play";
			rulesOp.text = "Rules";
			creditsOp.text = "Credits";
			exit.text = "Exit";

			playOp.fontSize = static_cast<int>(55.0f * drawScaleY);

			rulesOp.fontSize = playOp.fontSize;
			creditsOp.fontSize = playOp.fontSize;
			exit.fontSize = playOp.fontSize;
			title.fontSize = playOp.fontSize*2;

			playOp.posY = static_cast<int>(screenHeight) - playOp.fontSize * 4 - static_cast<int>(playOp.fontSize / 0.7f);
			rulesOp.posY = static_cast<int>(screenHeight) - playOp.fontSize * 3 - static_cast<int>(playOp.fontSize / 0.7f);
			creditsOp.posY = static_cast<int>(screenHeight) - playOp.fontSize * 2 - static_cast<int>(playOp.fontSize / 0.7f);
			exit.posY = static_cast<int>(screenHeight) - playOp.fontSize - static_cast<int>(playOp.fontSize / 0.7f);
			title.posY = static_cast<int>(title.fontSize);
		}

		static void setClickSound()
		{
			selectOption = true;
			soundMenuPlayed = true;
		}

		static void highlightMenuOption()
		{
			if (mouse.posY >= playOp.posY && mouse.posY <= playOp.posY + playOp.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(playOp.text, playOp.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(playOp.text, playOp.fontSize)) / 2 + MeasureText(playOp.text, playOp.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::GAMEPLAY;
				if (!soundMenuPlayed)
					setClickSound();
			}
			else if (mouse.posY >= rulesOp.posY && mouse.posY <= rulesOp.posY + rulesOp.fontSize &&
					mouse.posX >= (screenWidth - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2 + MeasureText(rulesOp.text, rulesOp.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::RULES;
				if (!soundMenuPlayed)
					setClickSound();
			}
			else if (mouse.posY >= creditsOp.posY && mouse.posY <= creditsOp.posY + creditsOp.fontSize &&
					mouse.posX >= (screenWidth - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2 + MeasureText(creditsOp.text, creditsOp.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::CREDITS;
				if (!soundMenuPlayed)
					setClickSound();
			}
			else if (mouse.posY >= exit.posY && mouse.posY <= exit.posY + exit.fontSize &&
					mouse.posX >= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 + MeasureText(exit.text, exit.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::EXIT;
				if (!soundMenuPlayed)
					setClickSound();
			}
			else
			{
				futurePlaceInGame = PLACEINGAME::NONE;
				if (soundMenuPlayed)
				{
					selectOption = false;
					soundMenuPlayed = false;
				}
				
			}
		}

		static void goToPlaceSelected()
		{
			if (mouse.posY >= playOp.posY && mouse.posY <= playOp.posY + playOp.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(playOp.text, playOp.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(playOp.text, playOp.fontSize)) / 2 + MeasureText(playOp.text, playOp.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::GAMEPLAY;
			}
			else if (mouse.posY >= rulesOp.posY && mouse.posY <= rulesOp.posY + rulesOp.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2 + MeasureText(rulesOp.text, rulesOp.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::RULES;
			}
			else if (mouse.posY >= creditsOp.posY && mouse.posY <= creditsOp.posY + creditsOp.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2 + MeasureText(creditsOp.text, creditsOp.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::CREDITS;
			}
			else if (mouse.posY >= exit.posY && mouse.posY <= exit.posY + exit.fontSize &&
				mouse.posX >= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(exit.text, exit.fontSize)) / 2 + MeasureText(exit.text, exit.fontSize))
			{
				currentPlaceInGame = PLACEINGAME::EXIT;
			}
			else
			{
				menuOptionChosen = false;
			}
		}

		static void setEnterSound()
		{
			enterPressed = true;
			soundMenuPlayed = true;
		}

		static void drawGameVersion()
		{
			gameVersion.text = "v1.0";
			gameVersion.fontSize = static_cast<int>(30 * drawScaleY);
			gameVersion.posY = screenLimit.down - gameVersion.fontSize - contourLineThickness;
			
			DrawText(gameVersion.text, (screenLimit.right - contourLineThickness - MeasureText(gameVersion.text, gameVersion.fontSize)),
				gameVersion.posY, gameVersion.fontSize, YELLOW);
		}

		static void drawPawsForWords(Words word)
		{
			float pawWidth = screenHeight / 21.0f;

			DrawTextureEx(paw.texture, { static_cast<float>((screenWidth - MeasureText(word.text, word.fontSize)) / 2 - 20 - pawWidth),
					static_cast<float>(word.posY + word.fontSize / 6) }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(paw.texture, { static_cast<float>((screenWidth + MeasureText(word.text, word.fontSize)) / 2 + 20),
				static_cast<float>(word.posY + word.fontSize / 6) }, 0.0f, 1.0f, WHITE);
		}

		static bool highlightScreenResolution()
		{
			if ((mouse.posY >= screenResolution[0].posY && mouse.posY <= screenResolution[0].posY + screenResolution[0].fontSize &&
				mouse.posX >= (screenWidth - MeasureText(screenResolution[0].text, screenResolution[0].fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(screenResolution[0].text, screenResolution[0].fontSize)) / 2 +
				MeasureText(screenResolution[0].text, screenResolution[0].fontSize)) ||
				((mouse.posY >= screenResolution[1].posY && mouse.posY <= screenResolution[1].posY + screenResolution[1].fontSize &&
					mouse.posX >= (screenWidth - MeasureText(screenResolution[1].text, screenResolution[1].fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(screenResolution[1].text, screenResolution[1].fontSize)) / 2 +
					MeasureText(screenResolution[1].text, screenResolution[1].fontSize))))
				return true;
			else
				return false;
		}

		static void changeScreenResolution()
		{
			short maxOp = 3;
			short minOp = 1;

			if ((mouse.posY >= screenResolution[0].posY && mouse.posY <= screenResolution[0].posY + screenResolution[0].fontSize &&
				mouse.posX >= (screenWidth - MeasureText(screenResolution[0].text, screenResolution[0].fontSize)) / 2 &&
				mouse.posX <= (screenWidth - MeasureText(screenResolution[0].text, screenResolution[0].fontSize)) / 2 +
				MeasureText(screenResolution[0].text, screenResolution[0].fontSize)) ||
				((mouse.posY >= screenResolution[1].posY && mouse.posY <= screenResolution[1].posY + screenResolution[1].fontSize &&
					mouse.posX >= (screenWidth - MeasureText(screenResolution[1].text, screenResolution[1].fontSize)) / 2 &&
					mouse.posX <= (screenWidth - MeasureText(screenResolution[1].text, screenResolution[1].fontSize)) / 2 +
					MeasureText(screenResolution[1].text, screenResolution[1].fontSize))))
			{
				screenResolutionChoice++;

				if (screenResolutionChoice > maxOp)
					screenResolutionChoice = minOp;

				CloseWindow();

				switch (screenResolutionChoice)
				{
				case 1:
					screenWidth = 800.0f;
					screenHeight = 450.0f; break;
				case 2:
					screenWidth = 1024.0f;
					screenHeight = 562.0f; break;
				case 3:
					screenWidth = 1360.0f;
					screenHeight = 765.0f; break;
				default:
					break;
				}

				InitWindow(static_cast<int>(screenWidth), static_cast<int>(screenHeight), title.text);

				drawScaleX = screenWidth / originalScreenWidth;
				drawScaleY = screenHeight / originalScreenHeight;

				screenLimit.down = static_cast<int>(screenHeight);
				screenLimit.right = static_cast<int>(screenWidth);

				loadTextures();

				menu::initialization();
				game::gameplay::initialization();
				game::rules::initialization();
				game::credits::initialization();

			}

		}

		static void drawScreenResolution()
		{
			Color color;

			screenResolution[0].text = "Screen resolution";

			screenResolution[0].fontSize = static_cast<int>(20.0f * drawScaleY);
			screenResolution[1].fontSize = screenResolution[0].fontSize;

			screenResolution[0].posY = static_cast<int>(exit.posY + exit.fontSize * 1.5f);
			screenResolution[1].posY = static_cast<int>(screenResolution[0].posY + screenResolution[0].fontSize * 1.2f);

			switch (screenResolutionChoice)
			{
			case 1: screenResolution[1].text = "800 x 450"; break;
			case 2: screenResolution[1].text = "1024 x 576"; break;
			case 3: screenResolution[1].text = "1360 x 765"; break;
			default:
				break;
			}

			if (!highlightScreenResolution())
				color = YELLOW;
			else
			{
				color = SKYBLUE;
				DrawText("Click to change!", static_cast<int>((screenWidth / 2.0f ) + MeasureText(screenResolution[1].text, screenResolution[1].fontSize) / 1.5f),
					screenResolution[1].posY, 20, color);
			}

			DrawText(screenResolution[0].text, (static_cast<int>(screenWidth) - MeasureText(screenResolution[0].text, screenResolution[0].fontSize)) / 2,
				screenResolution[0].posY, screenResolution[0].fontSize, color);
			DrawText(screenResolution[1].text, (static_cast<int>(screenWidth) - MeasureText(screenResolution[1].text, screenResolution[1].fontSize)) / 2,
				screenResolution[1].posY, screenResolution[1].fontSize, color);
			
		}

		//--------------------------------------------------------------

		void initialization()
		{
			returnToMenu = false;
			menuOptionChosen = false;
			currentPlaceInGame = PLACEINGAME::MENU;
			futurePlaceInGame = PLACEINGAME::MENU;
			drawingVarsInitialization();
		}

		void input()
		{
			mouse.posX = GetMouseX();
			mouse.posY = GetMouseY();

			if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				menuOptionChosen = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			highlightMenuOption();

			if (menuOptionChosen)
				changeScreenResolution();

			if (!inMenu)
			{
				inMenu = true;
				playMusic();
			}
			if (!mute)
			{
				UpdateMusicStream(menuStream);
			}
			playSound();
			if (selectOption)	selectOption = false;
			if (enterPressed)	enterPressed = false;

			if (menuOptionChosen)
			{
				goToPlaceSelected();
				menuOptionChosen = false;
				setEnterSound();
				soundMenuPlayed = false;
				inMenu = false;
			}
			
		}

		void draw()
		{
			const Color color = VIOLET;
			const Color selected = MAGENTA;

			ClearBackground(WHITE);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
			
			drawGameVersion();

			DrawText(title.text, (static_cast<int>(screenWidth) - MeasureText(title.text, title.fontSize)) / 2, title.posY, title.fontSize, BLACK);

			switch (futurePlaceInGame)
			{
			case configurations::PLACEINGAME::GAMEPLAY:
				DrawText(playOp.text, (static_cast<int>(screenWidth) - MeasureText(playOp.text, playOp.fontSize)) / 2, playOp.posY, playOp.fontSize, selected);
				DrawText(rulesOp.text, (static_cast<int>(screenWidth) - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2, rulesOp.posY, rulesOp.fontSize, color);
				DrawText(creditsOp.text, (static_cast<int>(screenWidth) - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2, creditsOp.posY, creditsOp.fontSize, color);
				DrawText(exit.text, (static_cast<int>(screenWidth) - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);

				drawPawsForWords(playOp);
				break;
			case configurations::PLACEINGAME::RULES:
				DrawText(playOp.text, (static_cast<int>(screenWidth) - MeasureText(playOp.text, playOp.fontSize)) / 2, playOp.posY, playOp.fontSize, color);
				DrawText(rulesOp.text, (static_cast<int>(screenWidth) - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2, rulesOp.posY, rulesOp.fontSize, selected);
				DrawText(creditsOp.text, (static_cast<int>(screenWidth) - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2, creditsOp.posY, creditsOp.fontSize, color);
				DrawText(exit.text, (static_cast<int>(screenWidth) - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);

				drawPawsForWords(rulesOp);
				break;
			case configurations::PLACEINGAME::CREDITS:
				DrawText(playOp.text, (static_cast<int>(screenWidth) - MeasureText(playOp.text, playOp.fontSize)) / 2, playOp.posY, playOp.fontSize, color);
				DrawText(rulesOp.text, (static_cast<int>(screenWidth) - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2, rulesOp.posY, rulesOp.fontSize, color);
				DrawText(creditsOp.text, (static_cast<int>(screenWidth) - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2, creditsOp.posY, creditsOp.fontSize, selected);
				DrawText(exit.text, (static_cast<int>(screenWidth) - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);

				drawPawsForWords(creditsOp);
				break;
			case configurations::PLACEINGAME::EXIT:
				DrawText(playOp.text, (static_cast<int>(screenWidth) - MeasureText(playOp.text, playOp.fontSize)) / 2, playOp.posY, playOp.fontSize, color);
				DrawText(rulesOp.text, (static_cast<int>(screenWidth) - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2, rulesOp.posY, rulesOp.fontSize, color);
				DrawText(creditsOp.text, (static_cast<int>(screenWidth) - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2, creditsOp.posY, creditsOp.fontSize, color);
				DrawText(exit.text, (static_cast<int>(screenWidth) - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, selected);

				drawPawsForWords(exit);
				break;
			case configurations::PLACEINGAME::NONE:
			default:
				DrawText(playOp.text, (static_cast<int>(screenWidth) - MeasureText(playOp.text, playOp.fontSize)) / 2, playOp.posY, playOp.fontSize, color);
				DrawText(rulesOp.text, (static_cast<int>(screenWidth) - MeasureText(rulesOp.text, rulesOp.fontSize)) / 2, rulesOp.posY, rulesOp.fontSize, color);
				DrawText(creditsOp.text, (static_cast<int>(screenWidth) - MeasureText(creditsOp.text, creditsOp.fontSize)) / 2, creditsOp.posY, creditsOp.fontSize, color);
				DrawText(exit.text, (static_cast<int>(screenWidth) - MeasureText(exit.text, exit.fontSize)) / 2, exit.posY, exit.fontSize, color);
				break;
			}

			drawScreenResolution();
		}

		void deinitialization()
		{

		}
	}
}