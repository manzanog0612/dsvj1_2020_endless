#include "scenes/gameplay.h"

#include <cmath>

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "elements/elements.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"
#include "sub_scenes/background.h"
#include "sub_scenes/objects.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace elements;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;
using namespace background;
using namespace objects;

namespace game
{
	namespace gameplay
	{
		static void restartGame()
		{
			if (floorsPosY[static_cast<int>(LINES::UP)] != static_cast<float>(screenHeight * 187.0f / 320.0f))
				floorsPosY[static_cast<int>(LINES::UP)] = static_cast<float>(screenHeight * 187.0f / 320.0f);
			if (floorsPosY[static_cast<int>(LINES::MIDDLE)] != static_cast<float>(screenHeight * 232.0f / 320.0f))
				floorsPosY[static_cast<int>(LINES::MIDDLE)] = static_cast<float>(screenHeight * 232.0f / 320.0f);
			if (floorsPosY[static_cast<int>(LINES::DOWN)] != static_cast<float>(screenHeight * 277.0f / 320.0f))
				floorsPosY[static_cast<int>(LINES::DOWN)] = static_cast<float>(screenHeight * 277.0f / 320.0f);

			player.active = false;
			player.actualLine = LINES::MIDDLE;
			player.dimentions.width = kitty.width;
			player.dimentions.height = kitty.height;
			player.dimentions.x = static_cast<float>(screenLimit.right / 12.0f);
			player.dimentions.y = floorsPosY[static_cast<int>(LINES::MIDDLE)] - kitty.height;
			player.score = 0;
			lose = false;

			defineObjects();
			defineImagesPosition();

			scoreToIncreaseSpeed = 5;

			soundLosingPLayed = false;
			soundLosing2PLayed = false;
		}

		static void definePlayer()
		{
			restartGame();
			player.controls.up = KEY_UP;
			player.controls.down = KEY_DOWN;
			player.controls.left = KEY_LEFT;
			player.controls.right = KEY_RIGHT;
			player.controls.jump = KEY_S;
			player.controls.dash = KEY_D;
			playerSpeed = 2;
		}

		static int addScore()
		{
			return 5;
		}

		static void setScore(int score)
		{
			player.score += score;
		}

		static void setSoundDash()
		{
			dash = true;
			soundDashPlayed = true;
		}

		static void setSoundJump()
		{
			jump = true;
			soundJumpPlayed = true;
		}

		static void setPlayerMovement()
		{
			switch (playerMovement)
			{
			case MOVEMENT::UP:

				if (!jumpInProcces && !dashInProcess)
				{
					switch (player.actualLine)
					{
					case LINES::MIDDLE: player.actualLine = LINES::UP;
						player.dimentions.y = floorsPosY[static_cast<int>(LINES::UP)] - kitty.height;
						break;
					case LINES::DOWN:	player.actualLine = LINES::MIDDLE;
						player.dimentions.y = floorsPosY[static_cast<int>(LINES::MIDDLE)] - kitty.height;
						break;
					default:break;
					}
				}
				break;

			case MOVEMENT::DOWN:

				if (!jumpInProcces && !dashInProcess)
				{
					switch (player.actualLine)
					{
					case LINES::UP:		player.actualLine = LINES::MIDDLE;
						player.dimentions.y = floorsPosY[static_cast<int>(LINES::MIDDLE)] - kitty.height;
						break;
					case LINES::MIDDLE: player.actualLine = LINES::DOWN;
						player.dimentions.y = floorsPosY[static_cast<int>(LINES::DOWN)] - kitty.height;
						break;
					default:break;
					}
				}
				break;

			case MOVEMENT::LEFT:

				if (!dashInProcess)
				{
					if (player.dimentions.x - playerSpeed >= screenLimit.left && player.dimentions.x - playerSpeed <= screenLimit.right)
						player.dimentions.x -= playerSpeed * 60 * GetFrameTime();
				}
				break;

			case MOVEMENT::RIGHT:

				if (!dashInProcess)
				{
					if (player.dimentions.x + playerSpeed >= screenLimit.left && player.dimentions.x + player.dimentions.width + playerSpeed <= screenLimit.right)
						player.dimentions.x += playerSpeed * 60 * GetFrameTime();
				}
				break;

			default:break;
			}
		}

		static float lerp(float a, float b, float f)
		{
			return (a * (1.0f - f)) + (b * f);
		}

		static void setJump()
		{
			if (!soundJumpPlayed)
				setSoundJump();

			if (!jumpUpDone)
			{
				if (startPos == 0.0f)
					startPos = player.dimentions.y;

				if (endPos == 0.0f)
					endPos = player.dimentions.y - 100;

				if (timeLaps < 1.0f)
				{
					timeLaps += 0.04f * 50.0f * GetFrameTime();
				}

				player.dimentions.y = lerp(startPos, endPos, timeLaps);

				if (timeLaps >= 1.0f)
				{
					jumpUpDone = true;
					timeLaps = 0.0f;
					startPos = 0.0f;
					endPos = 0.0f;
				}
			}

			if (jumpUpDone && !jumpDownDone)
			{
				if (startPos == 0.0f)
					startPos = player.dimentions.y;

				if (endPos == 0.0f)
					endPos = player.dimentions.y + 100;

				if (timeLaps < 1.0f)
				{
					timeLaps += 0.04f * 50.0f * GetFrameTime();
				}

				player.dimentions.y = lerp(startPos, endPos, timeLaps);

				if (timeLaps >= 1.0f)
				{
					jumpDownDone = true;
					timeLaps = 0.0f;
					startPos = 0.0f;
					endPos = 0.0f;
				}
			}

			if (jumpUpDone && jumpDownDone)
			{
				jumpInProcces = false;
				jumpDownDone = false;
				jumpUpDone = false;
				soundJumpPlayed = false;
			}
		}

		static void setDash()
		{
			if (!soundDashPlayed)
				setSoundDash();

			if (startPos == 0.0f)
				startPos = player.dimentions.x;

			if (endPos == 0.0f)
				endPos = player.dimentions.x + 70;

			if (timeLaps < 1.0f)
			{
				timeLaps += 0.1f * 50.0f * GetFrameTime();
			}

			if (lerp(startPos, endPos, timeLaps) + player.dimentions.width <= screenLimit.right)
				player.dimentions.x = lerp(startPos, endPos, timeLaps);

			if (timeLaps >= 1.0f)
			{
				dashInProcess = false;
				timeLaps = 0.0f;
				startPos = 0.0f;
				endPos = 0.0f;
				soundDashPlayed = false;
			}
		}

		static void drawScore()
		{
			short posX = screenLimit.left + 20;
			score.text = "Score: ";
			score.fontSize = static_cast<int>(25.0f * drawScaleY);
			score.posY = screenLimit.up + score.fontSize / 2;

			DrawText(score.text, posX, score.posY, score.fontSize, VIOLET);
			DrawText(FormatText("%i", player.score), posX + MeasureText(score.text, score.fontSize), score.posY, score.fontSize, VIOLET);
		}

		static void drawPauseScreen()
		{
			DrawTextureEx(defaultBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

			pause.text = "Pause";
			pressEnter.text = "Press enter to go back to menu";
			pressP.text = "Press P to coninue";
			pressM.text = "Press M to mute audio";

			pause.fontSize = static_cast<int>(80.0f * drawScaleY);
			pressEnter.fontSize = static_cast<int>(30.0f * drawScaleY);
			pressP.fontSize = pressEnter.fontSize;
			pressM.fontSize = pressEnter.fontSize;

			pause.posY = static_cast<int>(screenHeight) / 2 - pause.fontSize / 2;
			pressEnter.posY = screenLimit.down - pressEnter.fontSize * 2;
			pressP.posY = static_cast<int>(screenLimit.down / 20.0f + (10.0f * drawScaleY));
			pressM.posY = pressEnter.posY - pressM.fontSize * 2 - pressM.fontSize / 2;

			DrawText(pause.text, (screenLimit.right - MeasureText(pause.text, pause.fontSize)) / 2, pause.posY, pause.fontSize, VIOLET);

			DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, BLACK);

			DrawText(pressP.text, (screenLimit.right - MeasureText(pressP.text, pressP.fontSize)) / 2, pressP.posY, pressP.fontSize, BLACK);

			DrawText(pressM.text, (screenLimit.right - MeasureText(pressM.text, pressM.fontSize)) / 2, pressM.posY, pressM.fontSize, BLACK);
		}

		static void drawFirsIndications()
		{
			pressEnter.text = "Press enter to go back to menu";
			pressP.text = "Press P to pause";
			pressR.text = "Press R to restart the game";
			pressSpace.text = "Press space to start the game";

			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressP.fontSize = pressEnter.fontSize;
			pressR.fontSize = pressEnter.fontSize;
			pressSpace.fontSize = pressEnter.fontSize + static_cast<int>(10.0f * drawScaleY);

			pressEnter.posY = screenLimit.up + pressEnter.fontSize * 3;
			pressP.posY = pressEnter.posY + pressP.fontSize + static_cast<int>(10.0f * drawScaleY);
			pressR.posY = pressP.posY + pressR.fontSize + static_cast<int>(10.0f * drawScaleY);
			pressSpace.posY = pressEnter.posY - pressSpace.fontSize - static_cast<int>(10.0f * drawScaleY);

			if (!pauseGame)
			{
				DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, VIOLET);

				DrawText(pressP.text, (screenLimit.right - MeasureText(pressP.text, pressP.fontSize)) / 2, pressP.posY, pressP.fontSize, VIOLET);
			}

			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, VIOLET);

			if (!pauseGame)
			{
				DrawText(pressSpace.text, (screenLimit.right - MeasureText(pressSpace.text, pressSpace.fontSize)) / 2, pressSpace.posY, pressSpace.fontSize, MAGENTA);
			}
		}

		static void drawLosingScreen()
		{
			loser[0].text = "You lost";
			loser[1].text = "Wanna try again?";

			loser[0].fontSize = static_cast<int>(55.0f * drawScaleY);
			loser[1].fontSize = static_cast<int>(40.0f * drawScaleY);

			loser[0].posY = static_cast<int>(screenHeight / 6.0f);
			loser[1].posY = static_cast<int>(loser[0].posY + loser[0].fontSize + loser[1].fontSize / 4.0f);

			DrawTextureEx(loserScreen.texture, { screenWidth/4, screenHeight/8 }, 0.0f, 1.0f, WHITE);

			DrawText(loser[0].text, (static_cast<int>(screenWidth) - MeasureText(loser[0].text, loser[0].fontSize)) / 2,
				loser[0].posY, loser[0].fontSize, VIOLET);

			DrawText(loser[1].text, (static_cast<int>(screenWidth) - MeasureText(loser[1].text, loser[1].fontSize)) / 2,
				loser[1].posY, loser[1].fontSize, VIOLET);

			DrawText(pressR.text, (static_cast<int>(screenLimit.right) - MeasureText(pressR.text, pressR.fontSize)) / 2,
				pressR.posY + static_cast<int>(static_cast<float>(loser[0].fontSize) * 1.3f), pressR.fontSize, VIOLET);
		}

		//------------------------------------------------------------------------------

		void initialization()
		{
			definePlayer();
		}

		void input()
		{
			//player movement
			if (IsKeyPressed(player.controls.up))
				playerMovement = MOVEMENT::UP;
			else if (IsKeyPressed(player.controls.down))
				playerMovement = MOVEMENT::DOWN;
			else if (IsKeyPressed(player.controls.left) || IsKeyDown(player.controls.left))
				playerMovement = MOVEMENT::LEFT;
			else if (IsKeyPressed(player.controls.right) || IsKeyDown(player.controls.right))
				playerMovement = MOVEMENT::RIGHT;
			else playerMovement = MOVEMENT::NONE;

			if (IsKeyPressed(player.controls.jump) && !jumpInProcces && !dashInProcess)
				jumpInProcces = true;
			
			if (IsKeyPressed(player.controls.dash) && !dashInProcess && !jumpInProcces)
				dashInProcess = true;

			if (!player.active)
			{
				if (IsKeyPressed(KEY_SPACE))
					player.active = true;
			}

			if (IsKeyPressed(KEY_P)) pauseGame = !pauseGame;

			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_R))
				restart = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
				pauseGame = false;
				restartGame();
			}

			if (restart)
			{
				restart = false;
				restartGame();
				SetMusicVolume(gameplayStream, 0.1f);
			}

			if (!lose)
			{
				if (!pauseGame && player.active) //chequear donde inicializar los sonidos, por ahi en el arkanoid da una idea
				{
					setPlayerMovement();

					if (jumpInProcces)
						setJump();							
						
					if (dashInProcess)
						setDash();
					//background movement
					if (player.score >= scoreToIncreaseSpeed)
					{
						increaseBackgroundSpeed();

						if (player.score < 200)
							scoreToIncreaseSpeed *= 2;
						else
							scoreToIncreaseSpeed += 75;
					}
					setImagesMovement();
					//objects
					setObjectsUpdate();
				}
			}
			else
			{
				SetMusicVolume(gameplayStream, 0.05f);

				if (!soundLosingPLayed)
				{
					losingMatch = true;
					soundLosingPLayed = true;
				}

				if (!IsSoundPlaying(losing) && !losingMatch && !soundLosing2PLayed)
				{
					losingMatch2 = true;
					soundLosing2PLayed = true;
				}				
			}

			//audio
			if (!inGameplay)
			{
				inGameplay = true;
				playMusic();
			}
			if (!mute)
			{
				UpdateMusicStream(gameplayStream);
			}
			playSound();
			if (losingMatch)		losingMatch = false;
			if (losingMatch2)		losingMatch2 = false;
			if (enterPressed)		enterPressed = false;
			if (jump)				jump = false;
			if (dash)				dash = false;
			if (obstacleTouched)	obstacleTouched = false;
			if (treasureTouched)	treasureTouched = false;
		}

		void draw()
		{
			ClearBackground(WHITE);

			if (!pauseGame)
			{
				drawScenario();
				if (!player.active) drawFirsIndications();
				DrawTextureEx(kitty.texture, {player.dimentions.x, player.dimentions.y}, 0.0f, 1.0f, WHITE);

				#if DEBUG
					DrawLineEx({ 0.0f, floorsPosY[static_cast<int>(LINES::UP)] - 1.0f}, { static_cast<float>(screenLimit.right), 
						floorsPosY[static_cast<int>(LINES::UP)] - 1.0f }, 2, YELLOW);
					DrawLineEx({ 0.0f, floorsPosY[static_cast<int>(LINES::MIDDLE)] - 1.0f }, { static_cast<float>(screenLimit.right), 
						floorsPosY[static_cast<int>(LINES::MIDDLE)] - 1.0f }, 2, YELLOW);
					DrawLineEx({ 0.0f, floorsPosY[static_cast<int>(LINES::DOWN)] - 1.0f }, { static_cast<float>(screenLimit.right), 
						floorsPosY[static_cast<int>(LINES::DOWN)] - 1.0f },	2, YELLOW);
					DrawRectangleLinesEx(player.dimentions, 2, MAGENTA);
				#endif // DEBUG

				drawObject();
				drawScore();

				if (lose)
				{
					drawLosingScreen();
				}
			}
			else
			{
				drawPauseScreen();
				drawFirsIndications();
			}
		}

		void deinitialization()
		{
			
		}
	}
}
