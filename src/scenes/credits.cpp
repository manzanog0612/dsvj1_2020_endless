#include "scenes/credits.h"

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace credits
	{
		short actualScreen;
		const short maxScreen = 4;
		const short minScreen = 1;
		float arrowNextX;
		float arrowNextY;
		float arrowPreviousX;
		float arrowPreviousY;

		static void switchScreens()
		{
			Vector2 mouse;

			mouse.x = static_cast<float>(GetMouseX());
			mouse.y = static_cast<float>(GetMouseY());

			if (mouse.y >= arrowNextY && mouse.y <= arrowNextY + arrowNext.height &&
				mouse.x >= arrowNextX && mouse.x <= arrowNextX + arrowNext.width)
			{
				actualScreen++;
			}
			else if (mouse.y >= arrowPreviousY && mouse.y <= arrowPreviousY + arrowPrevious.height &&
					 mouse.x >= arrowPreviousX && mouse.x <= arrowPreviousX + arrowPrevious.width)
			{
				actualScreen--;
			}

			if (actualScreen < minScreen)
				actualScreen = maxScreen;
			else if (actualScreen > maxScreen)
				actualScreen = minScreen;
		}

		//----------------------------------------------------------------

		void initialization()
		{
			const short fontSize = static_cast<int>(20 * drawScaleY);

			creditsText[firstLine].text = "Audio";
			creditsText[secondLine].text = "From the page freesound.org, credits to:";
			creditsText[thirdLine].text = "PhonosUPF - \"click percussion\"";
			creditsText[fourthLine].text = "https://freesound.org/people/PhonosUPF/sounds/496064/";
			creditsText[fifthLine].text = "jeckkech - \"collision.wav\"";
			creditsText[sixthLine].text = "https://freesound.org/people/jeckkech/sounds/391658/";
			creditsText[seventhLine].text = "qubodup - \"Whoosh\"";
			creditsText[eighthLine].text = "https://freesound.org/people/qubodup/sounds/60013/";
			creditsText[ninethLine].text = "Taira Komori - \"destruction.mp3\"";
			creditsText[tenthLine].text = "https://freesound.org/people/Taira%20Komori/sounds/215593/";
			creditsText[eleventhLine].text = "dovidou - \"Boing.wav\"";
			creditsText[twelfthLine].text = "https://freesound.org/people/davidou/sounds/88451/";
			creditsText[thirteenthLine].text = "JonnyRuss01 - \"Click_1.wav\"";
			creditsText[fourteenthLine].text = "https://freesound.org/people/JonnyRuss01/sounds/478197/";
			creditsText[fifteenthLine].text = "Rocotilos - \"You Lose\"";
			creditsText[sixteenthLine].text = "https://freesound.org/people/Rocotilos/sounds/339835/";
			creditsText[seventeenthLine].text = "AdamWeeden - \"Die or Lose Life\"";
			creditsText[eighteenth].text = "https://freesound.org/people/AdamWeeden/sounds/157218/";
			creditsText[nineteenth].text = "From youtube, credits to:";
			creditsText[twentieth].text = "super noot ensemble - \"Memo | cute, 8 bit, chiptune\"";
			creditsText[twenty_firstLine].text = "https://www.youtube.com/watch?v=ox_T5Ia_Y0A";
			creditsText[twenty_secondLine].text = "Hazel Nut - \"8 bit Paradise - Royalty Free Kawaii Music(No copyright music)\"";
			creditsText[twenty_thirdLine].text = "https://www.youtube.com/watch?v=uFZ35E6qYvw";
			creditsText[twenty_fourthLine].text = "Textures";
			creditsText[twenty_fifthLine].text = "From pinterest, credits to:";
			creditsText[twenty_sixthLine].text = "Miri Tamis - \"kawaii living room\"";
			creditsText[twenty_seventhLine].text = "https://ar.pinterest.com/pin/366761963392805545/?nic_v2=1a9LL0ZBM";
			creditsText[twenty_eighthLine].text = "Natalie - \"Pixel scenes for everyone xD\"";
			creditsText[twenty_ninethLine].text = "https://ar.pinterest.com/pin/599541769135738333/?autologin=true&nic_v2=1a9LL0ZBM";
			creditsText[thirtiethLine].text = "All the rest of the assets were made by Guillermina Manzano";
			creditsText[thirty_firstLine].text = "Game desing and development";
			creditsText[thirty_secondLine].text = "by Guillermina Manzano";

			pressEnter.text = "Press enter to go back to menu";

			for (short i = firstLine; i <= eighteenth; i++)
			{
				if (i == firstLine)
					creditsText[i].posY = screenLimit.down / 24;
				else if (i == secondLine)
					creditsText[i].posY = static_cast<int>(creditsText[i - 1].posY + fontSize * 2.2f);
				else if (i == thirdLine)
					creditsText[i].posY = static_cast<int>(creditsText[i - 1].posY + fontSize * 2.1f);
				else
					creditsText[i].posY = static_cast<int>(creditsText[i - 1].posY + fontSize * 1.3f);
			}

			for (short i = nineteenth; i <= twenty_thirdLine; i++)
			{
				if (i == nineteenth)
					creditsText[i].posY = static_cast<int>(creditsText[i - nineteenth].posY + fontSize * 2.2f);
				else if (i == twentieth)
					creditsText[i].posY = static_cast<int>(creditsText[i - nineteenth].posY + fontSize * 2.1f);
				else
					creditsText[i].posY = static_cast<int>(creditsText[i - 1].posY + fontSize * 1.3f);
			}

			for (short i = firstLine; i <= seventhLine; i++)
			{
				if (i == firstLine)
					creditsText[i + twenty_fourthLine].posY = screenLimit.down / 24;
				else if (i == secondLine)
					creditsText[i + twenty_fourthLine].posY = static_cast<int>(creditsText[i + twenty_fourthLine - 1].posY + fontSize * 2.2f);
				else if (i == thirdLine)
					creditsText[i + twenty_fourthLine].posY = static_cast<int>(creditsText[i + twenty_fourthLine - 1].posY + fontSize * 2.1f);
				else
					creditsText[i + twenty_fourthLine].posY = static_cast<int>(creditsText[i + twenty_fourthLine - 1].posY + fontSize * 1.3f);
			}

			creditsText[thirty_firstLine].posY = static_cast<int>(screenLimit.down / 20.0f);
			creditsText[thirty_secondLine].posY = static_cast<int>(creditsText[thirty_secondLine - 1].posY + fontSize * 2.3f);

			actualScreen = 1;

			arrowNextX = screenWidth - arrowNext.width - arrowNext.width / 4.0f;      // - static_cast<float>(arrowNext.width) / 4.0f;
			arrowPreviousX = arrowNext.width / 4.0f;
			arrowNextY = screenHeight / 2.0f - arrowNext.width / 2.0f;
			arrowPreviousY = arrowNextY;
		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;

			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				switchScreens();
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			const short fontSize = static_cast<int>(20.0f * drawScaleY);
			const Color color = BLACK;
			pressEnter.posY = static_cast<int>(screenLimit.down - pressEnter.fontSize * 2.0f);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);

			ClearBackground(BLACK);

			DrawTextureEx(creditsBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(arrowNext.texture, { arrowNextX, arrowNextY }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(arrowPrevious.texture, { arrowPreviousX, arrowPreviousY }, 0.0f, 1.0f, WHITE);


			switch (actualScreen)
			{
			case 1:
				DrawText(creditsText[firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[firstLine].text, fontSize + 15)) / 2, creditsText[firstLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[secondLine].text, fontSize + 4)) / 2, creditsText[secondLine].posY, fontSize + 4, YELLOW);
				DrawText(creditsText[thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirdLine].text, fontSize)) / 2, creditsText[thirdLine].posY, fontSize, color);
				DrawText(creditsText[fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fourthLine].text, fontSize)) / 2, creditsText[fourthLine].posY, fontSize, color);
				DrawText(creditsText[fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fifthLine].text, fontSize)) / 2, creditsText[fifthLine].posY, fontSize, color);
				DrawText(creditsText[sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[sixthLine].text, fontSize)) / 2, creditsText[sixthLine].posY, fontSize, color);
				DrawText(creditsText[seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[seventhLine].text, fontSize)) / 2, creditsText[seventhLine].posY, fontSize, color);
				DrawText(creditsText[eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eighthLine].text, fontSize)) / 2, creditsText[eighthLine].posY, fontSize, color);
				DrawText(creditsText[ninethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[ninethLine].text, fontSize)) / 2, creditsText[ninethLine].posY, fontSize, color);
				DrawText(creditsText[tenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[tenthLine].text, fontSize)) / 2, creditsText[tenthLine].posY, fontSize, color);
				DrawText(creditsText[eleventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eleventhLine].text, fontSize)) / 2, creditsText[eleventhLine].posY, fontSize, color);
				DrawText(creditsText[twelfthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twelfthLine].text, fontSize)) / 2, creditsText[twelfthLine].posY, fontSize, color);
				DrawText(creditsText[thirteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirteenthLine].text, fontSize)) / 2, creditsText[thirteenthLine].posY, fontSize, color);
				DrawText(creditsText[fourteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fourteenthLine].text, fontSize)) / 2, creditsText[fourteenthLine].posY, fontSize, color);
				DrawText(creditsText[fifteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fifteenthLine].text, fontSize)) / 2, creditsText[fifteenthLine].posY, fontSize, color);
				DrawText(creditsText[sixteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[sixteenthLine].text, fontSize)) / 2, creditsText[sixteenthLine].posY, fontSize, color);
				DrawText(creditsText[seventeenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[seventeenthLine].text, fontSize)) / 2, creditsText[seventeenthLine].posY, fontSize, color);
				DrawText(creditsText[eighteenth].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eighteenth].text, fontSize)) / 2, creditsText[eighteenth].posY, fontSize, color);
				break;
			case 2:
				DrawText(creditsText[firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[firstLine].text, fontSize + 15)) / 2, creditsText[firstLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[nineteenth].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[nineteenth].text, fontSize + 4)) / 2, creditsText[nineteenth].posY, fontSize + 4, YELLOW);
				DrawText(creditsText[twentieth].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twentieth].text, fontSize)) / 2, creditsText[twentieth].posY, fontSize, color);
				DrawText(creditsText[twenty_firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_firstLine].text, fontSize)) / 2, creditsText[twenty_firstLine].posY, fontSize, color);
				DrawText(creditsText[twenty_secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_secondLine].text, fontSize)) / 2, creditsText[twenty_secondLine].posY, fontSize, color);
				DrawText(creditsText[twenty_thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_thirdLine].text, fontSize)) / 2, creditsText[twenty_thirdLine].posY, fontSize, color);
				break;
			case 3:
				DrawText(creditsText[twenty_fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_fourthLine].text, fontSize + 15)) / 2, creditsText[twenty_fourthLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[twenty_fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_fifthLine].text, fontSize + 4)) / 2, creditsText[twenty_fifthLine].posY, fontSize + 4, YELLOW);
				DrawText(creditsText[twenty_sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_sixthLine].text, fontSize)) / 2, creditsText[twenty_sixthLine].posY, fontSize, color);
				DrawText(creditsText[twenty_seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_seventhLine].text, fontSize)) / 2, creditsText[twenty_seventhLine].posY, fontSize, color);
				DrawText(creditsText[twenty_eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_eighthLine].text, fontSize)) / 2, creditsText[twenty_eighthLine].posY, fontSize, color);
				DrawText(creditsText[twenty_ninethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_ninethLine].text, fontSize)) / 2, creditsText[twenty_ninethLine].posY, fontSize, color);
				DrawText(creditsText[thirtiethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirtiethLine].text, fontSize)) / 2, creditsText[thirtiethLine].posY, fontSize, color);
				break;
			case 4:
				DrawText(creditsText[thirty_firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirty_firstLine].text, fontSize + 15)) / 2, creditsText[thirty_firstLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[thirty_secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirty_secondLine].text, fontSize + 7)) / 2, creditsText[thirty_secondLine].posY, fontSize + 7, color);
				break;
			default:
				break;
			}

			DrawText(pressEnter.text, (static_cast<int>(screenWidth) - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, VIOLET);
		}

		void deinitialization()
		{

		}
	}
}