#include "scenes/rules.h"

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace rules
	{
		void initialization()
		{
			const short fontSize = static_cast<int>(25.0f * drawScaleY);

			rulesText[firstLine].text = "You are a kitty running in a house!";
			rulesText[secondLine].text = "Your objetive is to collect as many points";
			rulesText[thirdLine].text = "as you can while avoiding the obstacles.";
			rulesText[fourthLine].text = "This objects will give you 1, 3 and 5 points";
			rulesText[fifthLine].text = "but be carefull with this guys!";
			rulesText[sixthLine].text = "You can jump on us with \"S\" or eliminate us with \"D\"";
			rulesText[seventhLine].text = "You can't jump on me nor eliminate me ";
			rulesText[eighthLine].text = "Move with the arrows!";

			pressEnter.text = "Press enter to go back to menu";

			rulesText[firstLine].posY = static_cast<int>(screenLimit.down / 15.0f);
			rulesText[secondLine].posY = static_cast<int>(rulesText[firstLine].posY + fontSize * 1.2f);
			rulesText[thirdLine].posY = static_cast<int>(rulesText[secondLine].posY + fontSize * 1.2f);
			rulesText[fourthLine].posY = static_cast<int>(rulesText[thirdLine].posY + fontSize * 3.0f);
			rulesText[fifthLine].posY = static_cast<int>(rulesText[fourthLine].posY + fontSize * 3.0f);
			rulesText[sixthLine].posY = static_cast<int>(rulesText[fifthLine].posY + fontSize * 3.5f);
			rulesText[seventhLine].posY = static_cast<int>(rulesText[sixthLine].posY + fontSize * 3.8f);
			rulesText[eighthLine].posY = static_cast<int>(rulesText[seventhLine].posY + fontSize * 2.0f);

		
		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			const Color color = BLACK;
			const short fontSize = static_cast<int>(25.0f * drawScaleY);
			float treasuresPosY = rulesText[fourthLine].posY + fontSize * 1.5f;

			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressEnter.posY = static_cast<int>(screenLimit.down - pressEnter.fontSize * 2);

			ClearBackground(BLACK);

			DrawTextureEx(defaultBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

			DrawText(rulesText[firstLine].text, (static_cast<int>(screenWidth)- MeasureText(rulesText[firstLine].text, fontSize))/ 2, rulesText[firstLine].posY, fontSize, color);
			DrawText(rulesText[secondLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[secondLine].text, fontSize)) / 2, rulesText[secondLine].posY, fontSize, color);
			DrawText(rulesText[thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[thirdLine].text, fontSize)) / 2, rulesText[thirdLine].posY, fontSize, color);
			DrawText(rulesText[fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[fourthLine].text, fontSize)) / 2, rulesText[fourthLine].posY, fontSize, color);

			DrawTextureEx(treasuresT[static_cast<int>(TREASURES::FISH)].texture, { static_cast<int>(screenWidth) / 2 - treasuresT[static_cast<int>(TREASURES::FISH)].width
				- treasuresT[static_cast<int>(TREASURES::WOOL)].width * 1.5f, treasuresPosY }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(treasuresT[static_cast<int>(TREASURES::WOOL)].texture, { static_cast<int>(screenWidth) / 2 - treasuresT[static_cast<int>(TREASURES::WOOL)].width
				/ 2.0f, treasuresPosY }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(treasuresT[static_cast<int>(TREASURES::MILK)].texture, { static_cast<int>(screenWidth) / 2 + treasuresT[static_cast<int>(TREASURES::WOOL)].width * 1.5f,
				treasuresPosY }, 0.0f, 1.0f, WHITE);

			DrawText(rulesText[fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[fifthLine].text, fontSize)) / 2, rulesText[fifthLine].posY, fontSize, color);

			DrawTextureEx(obstaclesT[static_cast<int>(OBSTACLES::MADCAT)].texture, { static_cast<int>(screenWidth) / 2 - obstaclesT[static_cast<int>(OBSTACLES::MADCAT)].width
				* 2.0f, rulesText[fifthLine].posY + fontSize * 1.1f }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(obstaclesT[static_cast<int>(OBSTACLES::DOG)].texture, { static_cast<float>(screenWidth / 2 + obstaclesT[static_cast<int>(OBSTACLES::DOG)].width),
				rulesText[fifthLine].posY + fontSize * 1.1f }, 0.0f, 1.0f, WHITE);

			DrawText(rulesText[sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[sixthLine].text, fontSize - 10)) / 2, rulesText[sixthLine].posY, fontSize - 10, color);

			DrawTextureEx(obstaclesT[static_cast<int>(OBSTACLES::BATHTUB)].texture, { screenWidth / 2 - obstaclesT[static_cast<int>(OBSTACLES::BATHTUB)].width
				/ 2.0f, rulesText[sixthLine].posY + (fontSize - 10) * 1.1f }, 0.0f, 1.0f, WHITE);

			DrawText(rulesText[seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[seventhLine].text, fontSize - 10)) / 2, rulesText[seventhLine].posY, fontSize - 10, color);
			DrawText(rulesText[eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[eighthLine].text, fontSize)) / 2, rulesText[eighthLine].posY, fontSize, color);

			DrawText(pressEnter.text, (static_cast<int>(screenWidth) - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, VIOLET);
		}

		void deinitialization()
		{
			
		}
	}
}