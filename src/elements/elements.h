#ifndef ELEMENTS_H
#define ELEMENTS_H

#include "raylib.h"

#include "elements_properties.h"
#include "configurations/configurations.h"

using namespace game;
using namespace elements_properties;
using namespace configurations;

namespace game
{
	namespace elements
	{
		namespace character
		{
			struct Character
			{
				Rectangle dimentions;
				Controls controls;
				LINES actualLine;
				short score = 0;
				bool active;
			};
		}
	}
}
#endif //ELEMENTS_H