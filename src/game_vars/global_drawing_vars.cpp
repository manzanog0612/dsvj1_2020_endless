#include "game_vars/global_drawing_vars.h"

#include "raylib.h"

#include "configurations/configurations.h"

using namespace game;
using namespace configurations;

namespace game
{
	namespace global_drawing_vars
	{
		float originalScreenWidth = 1000;
		float originalScreenHeight = 600;

		float drawScaleX = screenWidth / originalScreenWidth;
		float drawScaleY = screenHeight / originalScreenHeight;

		Words title;

		Words playOp;
		Words options;
		Words rulesOp;
		Words creditsOp;
		Words exit;

		Words rulesText[8];
		Words creditsText[32];

		Words pressEnter;
		Words pressP;
		Words pressR;
		Words pressM;
		Words pressSpace;

		Words score;

		Words pause;

		Words loser[2];

		Words youLost;

		Words gameVersion;

		Words screenResolution[2];
	}
}