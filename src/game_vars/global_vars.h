#ifndef GLOBAL_VARS_H
#define GLOBAL_VARS_H

#include "raylib.h"

#include "elements/elements.h"
#include "configurations/configurations.h"

using namespace game;
using namespace elements;
using namespace configurations;

namespace game
{
	namespace global_vars
	{
		struct Scenario
		{
			float speed;
			bool onSight;
			float position;
		};

		struct objectsGameplay
		{
			bool inGame;
			bool isObstacle;
			TREASURES treasureType;
			OBSTACLES obstacleType;
			Texture2D texture;
			Rectangle dimentions;
			LINES actualFloor;
		};

		extern character::Character player;

		extern short fps;

		extern MOVEMENT playerMovement;

		extern PLACEINGAME currentPlaceInGame;
		extern PLACEINGAME futurePlaceInGame;

		extern Mouse mouse;

		extern bool menuOptionChosen;

		extern bool returnToMenu;

		extern bool pauseGame;

		extern bool restart;

		extern short contourLineThickness;

		extern bool lose;

		const short amountFloors = 3;
		const short backgroundImages = 3;

		extern Scenario backgroundScenario[backgroundImages];
		extern Scenario middleScenario[backgroundImages];
		extern Scenario fowardScenario[backgroundImages];
		extern Scenario floorScenario[amountFloors];

		extern float floorsPosY[amountFloors];

		const short amountObjectsX = 4;
		const short amountObjectsY = 3;

		extern objectsGameplay objectsInGame[amountObjectsY][amountObjectsX];

		extern int scoreToIncreaseSpeed;

		extern bool jumpInProcces;
		extern bool jumpUpDone;
		extern bool jumpDownDone;
		extern bool dashInProcess;

		extern float startPos;
		extern float endPos;
		extern float timeLaps;

		extern short screenResolutionChoice;

		extern float playerSpeed;

		extern bool playingGame;
	}
}
#endif //GLOBAL_VARS_H
