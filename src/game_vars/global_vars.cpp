#include "game_vars/global_vars.h"

#include "elements/elements.h"
#include "game_vars/global_drawing_vars.h"

using namespace game;
using namespace elements;
using namespace global_drawing_vars;

namespace game
{
	namespace global_vars
	{
		character::Character player;

		short fps = 60;

		MOVEMENT playerMovement = MOVEMENT::NONE;

		PLACEINGAME currentPlaceInGame;
		PLACEINGAME futurePlaceInGame;

		Mouse mouse;

		bool menuOptionChosen = false;

		bool returnToMenu = false;

		bool pauseGame = false;

		bool restart = false;

		short contourLineThickness = static_cast<short>(12 * drawScaleX);

		bool lose;

		Scenario backgroundScenario[backgroundImages];
		Scenario middleScenario[backgroundImages];
		Scenario fowardScenario[backgroundImages];
		Scenario floorScenario[amountFloors];

		float floorsPosY[amountFloors];

		objectsGameplay objectsInGame[amountObjectsY][amountObjectsX];

		int scoreToIncreaseSpeed;

		bool jumpInProcces = false;
		bool jumpUpDone = false;
		bool jumpDownDone = false;
		bool dashInProcess = false;

		float startPos = 0.0f;
		float endPos = 0.0f;
		float timeLaps = 0.0f;

		short screenResolutionChoice = 2;

		float playerSpeed;

		bool playingGame = true;
	}
}
