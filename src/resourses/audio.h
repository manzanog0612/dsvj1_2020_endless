#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace game
{
	namespace audio
	{
		extern Sound click;
		extern Sound enter;
		extern Sound youLose;
		extern Sound losing;
		extern Sound jumping;
		extern Sound dashing;
		extern Sound collisionObstacles;
		extern Sound collisionTreasures;
		extern Music gameplayStream;
		extern Music menuStream;

		extern bool soundJumpPlayed;
		extern bool soundDashPlayed;
		extern bool soundCollisionObstaclePlayed;
		extern bool soundCollisionTreasurePlayed;
		extern bool soundLosingPLayed;
		extern bool soundLosing2PLayed;
		extern bool soundMenuPlayed;

		void loadAudio();

		void playSound();
		void playMusic();
	}
}

#endif // AUDIO_H
