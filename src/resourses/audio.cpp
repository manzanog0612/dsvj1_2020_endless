#include "resourses/audio.h"

#include "resourses/events.h"

using namespace game;
using namespace events;

namespace game
{
	namespace audio
	{
		Sound click;
		Sound enter;
		Sound youLose;
		Sound losing;
		Sound jumping;
		Sound dashing;
		Sound collisionObstacles;
		Sound collisionTreasures;
		Music gameplayStream;
		Music menuStream;

		bool soundJumpPlayed = false;
		bool soundDashPlayed = false;
		bool soundCollisionObstaclePlayed = false;
		bool soundCollisionTreasurePlayed = false;
		bool soundLosingPLayed = false;
		bool soundLosing2PLayed = false;
		bool soundMenuPlayed = false;
		
		void loadAudio()
		{
			click = LoadSound("res/assets/audio/sounds/click.mp3");
			enter = LoadSound("res/assets/audio/sounds/enter.mp3");
			youLose = LoadSound("res/assets/audio/sounds/youLose.mp3");
			losing = LoadSound("res/assets/audio/sounds/losing.mp3");
			jumping = LoadSound("res/assets/audio/sounds/jumping.mp3");
			dashing = LoadSound("res/assets/audio/sounds/dashing.mp3");
			collisionObstacles = LoadSound("res/assets/audio/sounds/collisionObstacles.mp3");
			collisionTreasures = LoadSound("res/assets/audio/sounds/collisionTreasures.mp3");
			gameplayStream = LoadMusicStream("res/assets/audio/music/gameplay.mp3");
			menuStream = LoadMusicStream("res/assets/audio/music/menu.mp3");
		}

		void playSound()
		{
			if (!mute && selectOption)		PlaySoundMulti(click);
			if (!mute && enterPressed)		PlaySoundMulti(enter);
			if (!mute && jump)				PlaySoundMulti(jumping);
			if (!mute && dash)				PlaySoundMulti(dashing);
			if (!mute && obstacleTouched)	PlaySoundMulti(collisionObstacles);
			if (!mute && treasureTouched)	PlaySoundMulti(collisionTreasures);
			if (!mute && losingMatch)		PlaySound(losing);
			if (!mute && losingMatch2)		PlaySound(youLose);

			SetSoundVolume(click, 0.5f);
			SetSoundVolume(enter, 0.5f);
			SetSoundVolume(losing, 0.1f);
			SetSoundVolume(youLose, 0.8f);
			SetSoundVolume(collisionObstacles, 0.2f);
			SetSoundVolume(collisionTreasures, 0.2f);
		}

		void playMusic()
		{
			if (inMenu) PlayMusicStream(menuStream);
			if (inGameplay) PlayMusicStream(gameplayStream);

			SetMusicVolume(menuStream, 0.1f);
			SetMusicVolume(gameplayStream, 0.1f);
		}

	}
}