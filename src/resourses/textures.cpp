#include "resourses/textures.h"

#include "raylib.h"

#include "configurations/configurations.h"
#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"

using namespace configurations;
using namespace global_vars;
using namespace global_drawing_vars;

namespace game
{
	namespace textures
	{
		Textures menuBackground;
		Textures defaultBackground;
		Textures creditsBackground;
		Textures loserScreen;
		Textures paw;
		Textures arrowNext;
		Textures arrowPrevious;
		Textures gameplayBackground[backgroundImages];
		Textures gameplayMiddle[backgroundImages];
		Textures gameplayFoward[backgroundImages];
		Textures floor[backgroundImages];
		Textures kitty;
		Textures treasuresT[3];
		Textures obstaclesT[3];

		void loadTextures()
		{
			//images
			Image gameplayBackgroundAux[backgroundImages];
			Image gameplayMiddleAux[backgroundImages];
			Image gameplayFowardAux[backgroundImages];
			Image floorAux[backgroundImages];
			Image treasuresTAux[3];
			Image obstaclesTAux[3];

			Image kittyAux = LoadImage("res/assets/textures/kitty.png");;
			Image arrowNextAux = LoadImage("res/assets/textures/arrowNext.png");
			Image arrowPreviousAux = LoadImage("res/assets/textures/arrowPrevious.png");
			Image menuBackgroundAux = LoadImage("res/assets/textures/menuBackgroundEndless.png");
			Image defaultBackgroundAux = LoadImage("res/assets/textures/backgroundEndless.png");
			Image loserScreenAux = LoadImage("res/assets/textures/loserScreen.png");
			Image pawAux = LoadImage("res/assets/textures/paw.png");
			Image creditsBackgroundAux = LoadImage("res/assets/textures/creditsScreen.png");
			gameplayBackgroundAux[0] = LoadImage("res/assets/textures/GMbackground-1.png");
			gameplayBackgroundAux[1] = LoadImage("res/assets/textures/GMbackground-2.png");
			gameplayBackgroundAux[2] = LoadImage("res/assets/textures/GMbackground-3.png");
			gameplayMiddleAux[0] = LoadImage("res/assets/textures/GMmiddle-1.png");
			gameplayMiddleAux[1] = LoadImage("res/assets/textures/GMmiddle-2.png");
			gameplayMiddleAux[2] = LoadImage("res/assets/textures/GMmiddle-3.png");
			gameplayFowardAux[0] = LoadImage("res/assets/textures/GMfoward-1.png");
			gameplayFowardAux[1] = LoadImage("res/assets/textures/GMfoward-2.png");
			gameplayFowardAux[2] = LoadImage("res/assets/textures/GMfoward-3.png");
			floorAux[0] = LoadImage("res/assets/textures/GMfloors-1.png");
			floorAux[1] = LoadImage("res/assets/textures/GMfloors-2.png");
			floorAux[2] = LoadImage("res/assets/textures/GMfloors-3.png");
			treasuresTAux[static_cast<int>(TREASURES::MILK)] = LoadImage("res/assets/textures/milk.png");
			treasuresTAux[static_cast<int>(TREASURES::WOOL)] = LoadImage("res/assets/textures/wool.png");
			treasuresTAux[static_cast<int>(TREASURES::FISH)] = LoadImage("res/assets/textures/fish.png");
			obstaclesTAux[static_cast<int>(OBSTACLES::BATHTUB)]= LoadImage("res/assets/textures/bathtub.png");
			obstaclesTAux[static_cast<int>(OBSTACLES::DOG)] = LoadImage("res/assets/textures/dog.png");
			obstaclesTAux[static_cast<int>(OBSTACLES::MADCAT)]= LoadImage("res/assets/textures/madCat.png");

			//images to images with pointer to resize
			Image* gameplayBackgroundAuxP[backgroundImages];
			Image* gameplayMiddleAuxP[backgroundImages];
			Image* gameplayFowardAuxP[backgroundImages];
			Image* floorAuxP[backgroundImages];
			Image* treasuresTAuxP[3];
			Image* obstaclesTAuxP[3];

			for (short i = 0; i < backgroundImages; i++)
			{
				gameplayBackgroundAuxP[i] = &gameplayBackgroundAux[i];
				gameplayMiddleAuxP[i] = &gameplayMiddleAux[i];
				gameplayFowardAuxP[i] = &gameplayFowardAux[i];
				floorAuxP[i] = &floorAux[i];
				treasuresTAuxP[i] = &treasuresTAux[i];
				obstaclesTAuxP[i] = &obstaclesTAux[i];
			}

			Image* kittyAuxP = &kittyAux;
			Image* arrowNextAuxP = &arrowNextAux;
			Image* arrowPreviousAuxP = &arrowPreviousAux;
			Image* menuBackgroundAuxP = &menuBackgroundAux;
			Image* defaultBackgroundAuxP = &defaultBackgroundAux;
			Image* loserScreenAuxP = &loserScreenAux;
			Image* pawAuxP = &pawAux;
			Image* creditsBackgroundAuxP = &creditsBackgroundAux;

			//width
			kitty.width = screenWidth / 22.0f;
			arrowNext.width = screenWidth / 10.0f;
			arrowPrevious.width = arrowNext.width;

			for (short i = 0; i < backgroundImages; i++)
			{
				if (i == static_cast<int>(OBSTACLES::BATHTUB)) //|| static_cast<int>(TREASURES::MILK))
				{
					obstaclesT[i].width = screenWidth / 12.0f;
					treasuresT[i].width = screenWidth / 20.0f;
				}
				else if (i == static_cast<int>(OBSTACLES::DOG)) //|| static_cast<int>(TREASURES::WOOL)
				{
					obstaclesT[i].width = screenWidth / 18.0f;
					treasuresT[i].width = screenWidth / 22.0f;
				}
				else // static_cast<int>(TREASURES::FISH) || static_cast<int>(OBSTACLES::MADCAT)
				{
					obstaclesT[i].width = screenWidth / 15.0f;
					treasuresT[i].width = screenWidth / 18.0f;
				}
			}

			//height
			kitty.height = screenHeight / 12;
			arrowNext.height = screenHeight / 8.0f;
			arrowPrevious.height = arrowNext.height;

			for (short i = 0; i < backgroundImages; i++)
			{
				if (i == static_cast<int>(OBSTACLES::BATHTUB)) //|| static_cast<int>(TREASURES::MILK))
				{
					treasuresT[i].height = screenHeight / 22.0f;
					obstaclesT[i].height = screenHeight / 8.0f;
				}
				else if (i == static_cast<int>(OBSTACLES::DOG)) //|| static_cast<int>(TREASURES::WOOL)
				{
					treasuresT[i].height = screenHeight / 22.0f;
					obstaclesT[i].height = screenHeight / 11.0f;
				}
				else // static_cast<int>(TREASURES::FISH) || static_cast<int>(OBSTACLES::MADCAT)
				{
					treasuresT[i].height = screenHeight / 22.0f;
					obstaclesT[i].height = screenHeight / 10.0f;
				}
			}

			//resizing
			for (short i = 0; i < backgroundImages; i++)
			{
				ImageResize(gameplayBackgroundAuxP[i], static_cast<int>(screenWidth), static_cast<int>(screenHeight));
				ImageResize(gameplayMiddleAuxP[i], static_cast<int>(screenWidth), static_cast<int>(screenHeight));
				ImageResize(gameplayFowardAuxP[i], static_cast<int>(screenWidth), static_cast<int>(screenHeight));
				ImageResize(floorAuxP[i], static_cast<int>(screenWidth), static_cast<int>(screenHeight));

				if (i == static_cast<int>(OBSTACLES::BATHTUB)) //|| static_cast<int>(TREASURES::MILK))
				{
					ImageResize(treasuresTAuxP[i], static_cast<int>(screenWidth / 20.0f), static_cast<int>(screenHeight / 22.0f));
					ImageResize(obstaclesTAuxP[i], static_cast<int>(screenWidth / 12.0f), static_cast<int>(screenHeight / 8.0f));
				}
				else if (i == static_cast<int>(OBSTACLES::DOG)) //|| static_cast<int>(TREASURES::WOOL)
				{
					ImageResize(treasuresTAuxP[i], static_cast<int>(screenWidth / 22.0f), static_cast<int>(screenHeight / 22.0f));
					ImageResize(obstaclesTAuxP[i], static_cast<int>(screenWidth / 18.0f), static_cast<int>(screenHeight / 11.0f));
				}
				else // static_cast<int>(TREASURES::FISH) || static_cast<int>(OBSTACLES::MADCAT)
				{
					ImageResize(treasuresTAuxP[i], static_cast<int>(screenWidth / 18.0f), static_cast<int>(screenHeight / 22.0f));
					ImageResize(obstaclesTAuxP[i], static_cast<int>(screenWidth / 15.0f), static_cast<int>(screenHeight / 10.0f));
				}
			}

			ImageResize(kittyAuxP, static_cast<int>(screenWidth / 22.0f), static_cast<int>(screenHeight / 12.0f));
			ImageResize(arrowNextAuxP, static_cast<int>(screenWidth / 10.0f), static_cast<int>(screenHeight / 8.0f));
			ImageResize(arrowPreviousAuxP, static_cast<int>(screenWidth / 10.0f), static_cast<int>(screenHeight / 8.0f));
			ImageResize(menuBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));
			ImageResize(defaultBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));
			ImageResize(loserScreenAuxP, static_cast<int>(screenWidth / 2.0f), static_cast<int>(screenHeight / 3.0f));
			ImageResize(pawAuxP, static_cast<int>(screenHeight / 21.0f), static_cast<int>(screenHeight / 18.0f));
			ImageResize(creditsBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));

			//transformation from image* to image 
			for (short i = 0; i < backgroundImages; i++)
			{
				gameplayBackgroundAux[i] = *gameplayBackgroundAuxP[i];
				gameplayMiddleAux[i] = *gameplayMiddleAuxP[i];
				gameplayFowardAux[i] = *gameplayFowardAuxP[i];
				floorAux[i] = *floorAuxP[i];
				treasuresTAux[i] = *treasuresTAuxP[i];
				obstaclesTAux[i] = *obstaclesTAuxP[i];
			}

			kittyAux = *kittyAuxP;
			arrowNextAux = *arrowNextAuxP;
			arrowPreviousAux = *arrowPreviousAuxP;
			menuBackgroundAux = *menuBackgroundAuxP;
			defaultBackgroundAux = *defaultBackgroundAuxP;
			loserScreenAux = *loserScreenAuxP;
			pawAux = *pawAuxP;
			creditsBackgroundAux = *creditsBackgroundAuxP;

			//transformation from image to texture2D
			for (short i = 0; i < backgroundImages; i++)
			{
				gameplayBackground[i].texture = LoadTextureFromImage(gameplayBackgroundAux[i]);
				gameplayMiddle[i].texture = LoadTextureFromImage(gameplayMiddleAux[i]);
				gameplayFoward[i].texture = LoadTextureFromImage(gameplayFowardAux[i]);
				floor[i].texture = LoadTextureFromImage(floorAux[i]);
			}

			menuBackground.texture = LoadTextureFromImage(menuBackgroundAux);
			defaultBackground.texture = LoadTextureFromImage(defaultBackgroundAux);
			loserScreen.texture = LoadTextureFromImage(loserScreenAux);
			paw.texture = LoadTextureFromImage(pawAux);
			creditsBackground.texture = LoadTextureFromImage(creditsBackgroundAux);
			kitty.texture = LoadTextureFromImage(kittyAux);
			treasuresT[static_cast<int>(TREASURES::MILK)].texture = LoadTextureFromImage(treasuresTAux[static_cast<int>(TREASURES::MILK)]);
			treasuresT[static_cast<int>(TREASURES::WOOL)].texture = LoadTextureFromImage(treasuresTAux[static_cast<int>(TREASURES::WOOL)]);
			treasuresT[static_cast<int>(TREASURES::FISH)].texture = LoadTextureFromImage(treasuresTAux[static_cast<int>(TREASURES::FISH)]);
			obstaclesT[static_cast<int>(OBSTACLES::BATHTUB)].texture = LoadTextureFromImage(obstaclesTAux[static_cast<int>(OBSTACLES::BATHTUB)]);
			obstaclesT[static_cast<int>(OBSTACLES::DOG)].texture = LoadTextureFromImage(obstaclesTAux[static_cast<int>(OBSTACLES::DOG)]);
			obstaclesT[static_cast<int>(OBSTACLES::MADCAT)].texture = LoadTextureFromImage(obstaclesTAux[static_cast<int>(OBSTACLES::MADCAT)]);
			arrowNext.texture = LoadTextureFromImage(arrowNextAux);
			arrowPrevious.texture = LoadTextureFromImage(arrowPreviousAux);
		}
	}
}