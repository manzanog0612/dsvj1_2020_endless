#ifndef EVENTS_H
#define EVENTS_H

namespace game
{
	namespace events
	{
		extern bool selectOption;
		extern bool enterPressed;
		extern bool losingMatch;
		extern bool losingMatch2;
		extern bool jump;
		extern bool dash;
		extern bool treasureTouched;
		extern bool obstacleTouched;
		extern bool inMenu;
		extern bool inGameplay;
		extern bool mute;
	
		void deactivateEvents();
	}
}

#endif // EVENTS_H