#include "resourses/events.h"

namespace game
{
	namespace events
	{
		bool selectOption = false;
		bool enterPressed = false;
		bool losingMatch = false;
		bool losingMatch2 = false;
		bool jump = false;
		bool dash = false;
		bool treasureTouched = false;
		bool obstacleTouched = false;
		bool inMenu = false;
		bool inGameplay = false;
		bool mute = false;

		void deactivateEvents()
		{
			if (losingMatch)		losingMatch = false;
			if (losingMatch2)		losingMatch2 = false;
			if (enterPressed)		enterPressed = false;
			if (jump)				jump = false;
			if (dash)				dash = false;
			if (obstacleTouched)	obstacleTouched = false;
			if (treasureTouched)	treasureTouched = false;
		}
	}
}