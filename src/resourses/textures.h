#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

#include "game_vars/global_vars.h"

using namespace global_vars;

namespace game
{
	namespace textures
	{
		struct Textures
		{
			Texture2D texture;
			float width;
			float height;
		};

		extern Textures menuBackground;
		extern Textures defaultBackground;
		extern Textures creditsBackground;
		extern Textures loserScreen;
		extern Textures paw;
		extern Textures arrowNext;
		extern Textures arrowPrevious;
		extern Textures gameplayBackground[backgroundImages];
		extern Textures gameplayMiddle[backgroundImages];
		extern Textures gameplayFoward[backgroundImages];
		extern Textures floor[backgroundImages];
		extern Textures kitty;
		extern Textures treasuresT[3];
		extern Textures obstaclesT[3];

		void loadTextures();
	}
}

#endif // TEXTURES_H
